const createError = require('http-errors');
const express = require('express');
const logger = require('./logger');

const User = require('./lib/models/user');
const { connectDb } = require('./lib/db/mongo-connector');

const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');
const accountRouter = require('./routes/account');
const accountRoleRouter = require('./routes/accountRole');
const eventsRouter = require('./routes/events');
const conversationRouter = require('./routes/conversation');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(logger.middleware);

app.use('/api', indexRouter);
app.use('/api/user', userRouter);
app.use('/api/account', accountRouter);
app.use('/api/accountrole', accountRoleRouter);
app.use('/api/events', eventsRouter);
app.use('/api/conversations', conversationRouter);


// catch 404 and forward to error handler
// app.use((req, res, next) => {
//     next(createError(404));
// });

// // error handler
// app.use((err, req, res) => {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};

//     // render the error page
//     res.status(err.status || 500);
//     res.send(JSON.stringify(err));
// });

// Check DB connection and initialize the DB
const init = true;
connectDb().then(async () => {
    if (init) {
        await Promise.all([
            User.insertMany(
                [{ firstName: 'Colin', lastName: 'Thomas', email: 'colin@avaamo.com', admin: false },
                { firstName: 'Deepima', lastName: 'Dwivedi', email: 'deepima.dwivedi@avaamo.com', admin: false },
                { firstName: 'Colin', lastName: 'Admin', email: 'colin+admin@avaamo.com', admin: true}
                ]
            )
        ]);
    }
    logger.logger('app').info('Connected to Mongo');
});

module.exports = app;
