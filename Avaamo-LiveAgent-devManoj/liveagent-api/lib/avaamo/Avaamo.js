var qs = require('qs');
var fetch = require('node-fetch');
var _ = require('lodash');

let instance = process.env.INSTANCE || "c0";

async function acceptChat(id, liveAgentToken) {
    var param = {
        access_token: liveAgentToken,
    }

    var endpoint = `https://${instance}.avaamo.com/live_agent/custom/conversations/${id}/accept_chat.json?${qs.stringify(param)}`;

    await fetch(endpoint, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((res) => {
        console.log("Ressss=>>>.", res);
        return res.text();
    });
}

async function disconnect (id, liveAgentToken) {
    var param = {
        access_token: liveAgentToken,
    }

    var endpoint = `https://${instance}.avaamo.com/live_agent/custom/conversations/${id}/end_chat.json?${qs.stringify(param)}`;

    await fetch(endpoint, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((res) => {
        return res.text();
    });
}

async function sendMessage (id, message, liveAgentToken) {
    // logger.info('Sending chat');
    var param = {
        access_token: liveAgentToken,
    }

    var endpoint = `https://${instance}.avaamo.com/live_agent/custom/messages.json?${qs.stringify(param)}`;
    var body = {
        message: {
            conversation: {
                uuid: id
            },
            content: message,
            content_type: 'text'
        }
    }

    await fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((res) => {
        console.log("API Response====>>>", res);
        return res.text();
    });
}

async function listOfRequests(id, liveAgentToken) {
    var param = {
        access_token: liveAgentToken,
    }

    var endpoint = `https://${instance}.avaamo.com/live_agent/custom/conversations/${id}/accept_chat.json?${qs.stringify(param)}`;

    await fetch(endpoint, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((res) => {
        console.log("Ressss=>>>.", res);
        return res.text();
    });
}


module.exports = { acceptChat, disconnect, sendMessage, listOfRequests };