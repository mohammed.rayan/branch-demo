const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const ejs = require('ejs');
const path = require('path');

const logger = require('../../logger').logger('lib/mailer');

require('dotenv').config();

async function sendmail(emailTo, template, data) {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport(
        nodemailerSendgrid({
            apiKey: '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'
        })
    );

    ejs.renderFile(path.resolve(__dirname, `templates/${template}.ejs`), data, (ex, data) => {
        if (ex) {
            logger.error(ex);
        } else {
            var mainOptions = {
                from: '"Avaamo" no-reply@avaamo.com',
                to: emailTo,
                subject: 'Authentication',
                html: data
            };

            transporter.sendMail(mainOptions, (ex) => {
                if (ex)
                    logger.error(ex);
            });
        }
    });
}

module.exports = sendmail;