var mongoose = require('mongoose');

require('dotenv').config();

const connectDb = () => {
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useUnifiedTopology', true);
    mongoose.set('useFindAndModify', false);
    

   return mongoose.connect(process.env.DATABASE_URL);
};

module.exports = { connectDb };