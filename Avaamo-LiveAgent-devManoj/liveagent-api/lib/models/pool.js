const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var PoolSchema = new Schema({
    conversations: { type: Schema.Types.ObjectId, ref: 'Conversation' },
    account: { type: Schema.Types.ObjectId, ref: 'Account' },
    accepted: Boolean,
    closed: Boolean
});

PoolSchema.statics = {
    list: function (account) {
        return this.find({ account: account })
            .exec();
    }
};

module.exports = mongoose.model('Pool', PoolSchema);