const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var MessageSchema = new Schema({
    conversation: { type: Schema.Types.ObjectId, ref: 'Conversation' },
    message: [{
        text: String,
        from: String,
        createdAt: { type: Date, default: Date.now },
    }]
});

// MessageSchema.methods = {
//     create: function () {
//         const err = this.validateSync();
//         if (err && err.toString())
//             throw new Error(err.toString());
//         this.save();
//     }
// };

MessageSchema.statics = {
    create: function (conversationId, message) {
        return this.findOneAndUpdate({ conversation: conversationId },
            {
                "$addToSet": { "message" : message },
                "$set": {
                    "conversation": conversationId,
                }
            }, { upsert: true }, (err, res) => {
                if(res) {
                    console.log('res',res);
                    return res;
                }
            });
    },
    listOfMessages: function (conversationId) {
        return this.find({ conversation: conversationId })
            .exec();
    },
};

module.exports = mongoose.model('Message', MessageSchema);