const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    admin: Boolean,
    pin: String,
    token: { type: String, select: false },
    createdAt: Date,
    lastLogin: Date,
    workHours: String,
    timezone: String,
    filter: String,
    status: { type: Boolean, default: false }
});

UserSchema.path('firstName').required(true, 'First name cannot be blank');
UserSchema.path('email').required(true, 'Email cannot be blank');
UserSchema.path('admin').required(true, 'Admin cannot be blank');

UserSchema.methods = {
    create: function () {
        const err = this.validateSync();
        if (err && err.toString())
            throw new Error(err.toString());

        this.save();
    }
};

UserSchema.statics = {
    findById: function (_id) {
        return this.findOne({ _id })
            .exec();
    },
    findByEmail: function (email) {
        return this.findOne({ email: email })
            .exec();
    },
    findByToken: function (token) {
        return this.findOne({ token: token })
            .exec();
    }
};

module.exports = mongoose.model('User', UserSchema);