const express = require('express');
const router = express.Router();
const _ = require('lodash');
const User = require('../lib/models/user');
const Account = require('../lib/models/account');
const AccountRole = require('../lib/models/accountRole');
const UserAccount = require('../lib/models/userAccount');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

router.post('/:id/create', async (req, res) => {
    const token = req.header('Access-Token');
    const accountId = req.params.id;
    const roles = req.body.roles;

    const u = await User.findByToken(token);
    const account = await Account.find(accountId);

    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);

    if (_.isNull(account))
        return res.sendStatus(401);

    const accountRole = new AccountRole();
    accountRole.account = account;
    accountRole.roles = roles;
    accountRole.create();
    return res.json(accountRole).status(201);
});

// find All Roles by account id
router.get('/account/:id', async (req, res) => {
    const token = req.header('Access-Token');
    const accountId = req.params.id;

    const u = await User.findByToken(token);
    const account = await Account.find(accountId);

    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);

    if (_.isNull(account))
        return res.sendStatus(401);

    const accountRole = await AccountRole.findByAccountId(accountId);  
    return res.json(accountRole).status(200);
});


router.get('/:id', async (req, res) => {
    const token = req.header('Access-Token');
    const roleId = req.params.id;

    const u = await User.findByToken(token);

    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);

    // TODO: find All by account id
    const accountRole = await AccountRole.findByRoleId(roleId);  
    return res.json(accountRole).status(200);
});


router.put('/:id', async (req, res) => {
    const token = req.header('Access-Token');
    const roleId = req.params.id;

    const u = await User.findByToken(token);

    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);
    let account = await AccountRole.update({ _id : roleId }, { $set: req.body }).exec();

    return res.json(account).status(200);
});

router.delete('/:id', async (req, res) => {
    const token = req.header('Access-Token');
    const roleId = req.params.id;

    const u = await User.findByToken(token);

    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);

    let account = await AccountRole.deleteOne({ _id : roleId }, { $set: req.body }).exec();

    return res.json(account).status(200);
});

module.exports = router;
